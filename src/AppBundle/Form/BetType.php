<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class BetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('disciplin', ChoiceType::class, array(
                'choices' => array(
                    'Piłka nożna' => 'Piłka nożna',
                    'Siatkówka' => 'Siatkówka',
                    'Koszykówka' => 'Koszykówka',
                ),
            ))
            ->add('options')
            ->add('courses')
//            ->add('result')
            ->add('status')
//            ->add('createdAt')
//            ->add('user')
            ->add('date');


    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Bet'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_bet';
    }


}
