<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Bet;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Bet');

        $bets = $repository->getBets(10);
//        echo '<pre>';
//        var_dump($bets["courses"]);
//        echo '</pre>';

        return $this->render('default/index.html.twig', array(
            'bets' => $bets,));
    }
    /**
     * @Route("/blog", name="blog")
     */
    public function blogAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Post');

        $query = $repository->getPosts(50);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('default/blog.html.twig', ['posts' => $pagination]);
    }
    /**
     * @Route("/article/{id}", name="post_show")
     *
     */
    public function showAction(Post $post, Request $request)
    {
        $form = null;

        if ($user = $this->getuser()) {
            $comment = new Comment();
            $comment->setPost($post);
            $comment->setUser($user);

            $form = $this->createForm('AppBundle\Form\CommentType', $comment);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($comment);
                $em->flush();

                return $this->redirectToRoute('post_show', array('id' => $post->getId()));
            }
        }
        return $this->render('default/showPost.html.twig', array(
            'post' => $post,
            'form' => is_null($form) ? $form : $form->createView()
        ));
    }

    /**
     * @Route("/admin/new_bet", name="new_bet")
     *
     */
    public function newBet(Request $request)
    {
        $form = null;

        if ($user = $this->getuser()) {
            $bet = new Bet();
            $bet->setUser($user);

            $form = $this->createForm('AppBundle\Form\BetType', $bet);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($bet);
                $em->flush();

                return $this->redirectToRoute('new_bet');
            }
        }
        return $this->render('default/newBet.html.twig', array(
            'user' => $user,
            'form' => is_null($form) ? $form : $form->createView()
        ));
    }
}
